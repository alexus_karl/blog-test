# Blog-Test

This blog repository is for Coretech Integrated Solutions Inc [Coding Exam].


## Requirements and installation

1. Clone this repository : `git clone https://gitlab.com/alexus_karl/blog-test`
2. Install requirements.txt : `pip install -r requirements.txt`
3. Make migrations: 
 - `python manage.py makemigrations`
 - `python manage.py migrate`
3. Run server : `python manage.py runserver`

## Register your credentials to access the blog or redirect to Admin by Creating Superuser

1. `127.0.0.1/8000/register`

or

1. `python manage.py createsuperuser`